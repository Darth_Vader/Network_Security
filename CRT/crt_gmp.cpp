#include <bits/stdc++.h>
#include <gmp.h>


using namespace std;

void inverse(mpz_t M, mpz_t m, mpz_t inv)
{
	mpz_t i, one, temp;
	mpz_inits(i,one,temp,NULL);
	mpz_set_str(i,"1",10);
	mpz_set_str(one,"1",10);
	while(true)
	{
		if( mpz_cmp(i,m) < 0)
		{
			mpz_mul(temp, i, M);
			mpz_mod(temp,temp,m);
			if( mpz_cmp(temp,one) == 0)
			{
				mpz_set(inv,i);
				return;
			}
		}
		else
			break;
		mpz_add(i,i,one);
	}
}

int main()
{
	string x;
	int n;
	cout<<"\nEnter number of equations : ";
	cin>>n;
	cin.clear();
	cin.ignore();

	mpz_t *a, *m, ans,temp, M, temp_inv;
	
	mpz_inits(M,temp,temp_inv,ans,NULL);
	mpz_set_str(temp,"1",10);
	mpz_set_str(ans,"0",10);
	mpz_set(M,temp);

	a = new mpz_t[n];
	m = new mpz_t[n];

	for(int i=0; i<n; i++)
	{
		mpz_init2(a[i],1024);
		mpz_init2(m[i],1024);
		mpz_inits(a[i],m[i],NULL);
		cout<<"\nEnter a "<<i+1<<" : ";
		getline(cin,x);
		mpz_set_str(a[i],x.c_str(),10);
		cout<<"Enter m  "<<i+1<<" : ";
		getline(cin,x);
		mpz_set_str(m[i],x.c_str(),10);
		mpz_mul(M,M,m[i]);
	}


	for(int i=0; i<n; i++)
	{
		mpz_cdiv_q(temp, M,m[i]);
		inverse(temp,m[i],temp_inv);
		mpz_mul(temp,temp,a[i]);
		mpz_mul(temp,temp_inv,temp);
		mpz_add(ans,ans,temp);
	}

	mpz_mod(ans,ans,M);

	gmp_printf("\nans is : %Zd\n",ans);
	
	return 0;

}
