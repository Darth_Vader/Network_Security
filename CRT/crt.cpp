#include <bits/stdc++.h>

using namespace std;

int inverse(int M, int m)
{
	for(int i=1;i<m;i++)
	{
		if( (M*i)%m == 1)
			return i;
	}
	return -1;
}

int main()
{
	int n;
	cout<<"\nEnter number of equations : ";
	cin>>n;

	int *a, *m;
	a = new int[n];
	m = new int[n];

	for(int i=0; i<n; i++)
	{
		cout<<"\nEnter a["<<i+1<<"] : and m["<<i+1<<"] : ";
		cin>>a[i]>>m[i];
	}

	int M = 1;
	for(int i=0; i<n; i++)
		M *= m[i];

	int ans = 0;
	for(int i=0; i<n; i++)
	{
		int y = inverse(M/m[i],m[i]);
		if( y == -1)
		{
			cout<<"\nNot possible : ";
			exit(0);
		}
		ans = ans + y*a[i]*M/m[i];
	}
	cout<<"\nNumber is : "<<ans%M<<endl;
		
	return 0;
}
