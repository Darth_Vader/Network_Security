#include <bits/stdc++.h>

using namespace std;

string left_shift(string s)
{
	return s.substr(1,s.length()) + "0";
}

string xor_(string a, string b)
{
	for(int i=0; i<a.length(); i++)
	{
		if( a[i] == b[i])
			a[i] = '0';
		else
			a[i] = '1';
	}
	return a;
}

string fast_mul(string p1, string p2, string m)
{
	string temp;
	string ans;
	temp = p1;
	if( p2[p2.length() -1] == '1')
		ans = p1;
	else
		ans = string(p1.length(),'0');
	for(int i=1; i<m.length(); i++)
	{
		string x = left_shift(temp);
		if( temp[0] == '1')
			x = xor_(x,m);
		temp = x;
		if( p2[p2.length() - 1 - i] == '1')
			ans = xor_(ans,temp);
	}
	return ans;
}

int main()
{
	int n;
	string p1, p2, m;
	cout<<"\nEnter p1 : ";
	getline(cin,p1);
	cout<<"\nEnter p2 : ";
	getline(cin,p2);
	cout<<"\nEnter m : ";
	getline(cin,m);
	cout<<"\n Final ans : "<<fast_mul(p1,p2,m)<<endl;
	return 0;
}
