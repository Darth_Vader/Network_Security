#include <stdio.h>
#include <gmp.h>
#include <time.h>

void ex_gcd(mpz_t a, mpz_t b, mpz_t x, mpz_t y, mpz_t ans)
{

	mpz_t temp,zero,one, x1, y1,A, B;
	mpz_inits(temp,zero,A,B,one,x1,y1,NULL);

	mpz_set_str(zero,"0",10);
	mpz_set_str(one,"1",10);
	mpz_set(A,a);
	mpz_set(B,b);


	if( mpz_sgn(a) == 0)
	{
		mpz_set(x,zero);
		mpz_set(y,one);
		mpz_set(ans,b);
		return;
	}


	mpz_mod(temp,B,A);
	ex_gcd(temp,a,x1,y1,ans);

	mpz_set(y,x1);
	mpz_fdiv_q(temp,B,A);
	mpz_mul(temp,temp,x1);
	mpz_sub(x,y1,temp);
}

int main()
{
	mpz_t a,b,x,y,gcd,temp,temp2;
	unsigned long seed;
	gmp_randstate_t r_state;

	mpz_inits(a,b,x,y,gcd,temp,temp2,NULL);

	seed = time(NULL);
	
	gmp_randinit_default(r_state);
	gmp_randseed_ui(r_state,seed);

	mpz_urandomb(a,r_state,25);
	mpz_urandomb(b,r_state,25);

	gmp_printf("\na is :\n%Zd \n\nb is :\n%Zd\n",a,b);

	ex_gcd(a,b,x,y,gcd);

	gmp_printf("\nx = %Zd, y= %Zd \nGCD = %Zd\n",x,y,gcd);

	mpz_gcd(gcd,a,b);
	gmp_printf("\nGMP ans is : %Zd\n",gcd);

	mpz_mul(temp,a,x);
	mpz_mul(temp2,b,y);
	mpz_add(temp,temp,temp2);
	gmp_printf("\nx*a + y*b is : %Zd\n",temp);

	return 0;	


}
