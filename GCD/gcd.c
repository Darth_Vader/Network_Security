#include <stdio.h>
#include <gmp.h>
#include <time.h>

void gcd(mpz_t a, mpz_t b,mpz_t temp)
{
	mpz_t zero;
	mpz_init(zero);
	mpz_set_str(zero,"0",10);
	while( mpz_cmp(a,zero) != 0 )
	{
		mpz_set(temp,a);
		mpz_mod(a,b,a);
		mpz_set(b,temp);
		
	}
	printf("\nGCD is : ");
	mpz_out_str(stdout,10,b);	
	printf("\n");
}

int main()
{
	mpz_t a,b,t;
	unsigned long seed;
	gmp_randstate_t r_state;

	seed = time(NULL);

	gmp_randinit_default(r_state);
	gmp_randseed_ui(r_state,seed);

	mpz_inits(a,b,t,NULL);

	mpz_urandomb(a,r_state,1000);
	mpz_urandomb(b,r_state,1000);

	gmp_printf("\na is :\n%Zd \n\nb is :\n%Zd\n",a,b);


	gcd(a,b,t);
	mpz_gcd(t,a,b);
	gmp_printf("\nActual ans : %Zd\n",t);
	return 0;	


}
