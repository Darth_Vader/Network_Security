#include <bits/stdc++.h>

using namespace std;

int PC1[56];		// for key
int PC2[48];		// for key permutation
int IP_[64];		// initial permutation of block
int IP_INV[64];
int E[48];
int shifts_in_iterations[16];
int S[8][4][16];
int P[32];

unsigned char* left_shift(unsigned char* arr,int len)
{
	unsigned char *shifted = new unsigned char[len];
	bool bit = false;
	
	for(int i=len-1; i>=0; i--)
	{
		bool temp_bit = arr[i] & 0x80;
		shifted[i] = (arr[i] << 1 ) | bit;
		bit = temp_bit;
	}
	return shifted;
}

unsigned char* right_shift(unsigned char* arr, int len)
{
	unsigned char *shifted = new unsigned char[len];
	unsigned char bit = 0x00;
	for(int i=0;i<len;i++)
	{
		unsigned char temp_bit = arr[i] & 0x01;
		shifted[i] = (arr[i] >>1 ) | (bit << 7);
		bit = temp_bit;
	}
	return shifted;
}

void to_binary(unsigned char c)
{
	for(int i=0;i<8;i++)
	{
		if( c & (1<<(7-i)))
			cout<<1;
		else
			cout<<0;
		if( i == 3)
			cout<<"_";
	}
}

void print(unsigned char* x,int len)
{
	for(int i=0; i<len;i++)
	{
		to_binary(x[i]);
		cout<<" ";
	}
}

unsigned char* hex_to_ascii(string str)
{
	unsigned char* arr = new unsigned char[ (str.length()+1)/2 ];
	memset(arr,0,(str.length()+1)/2);
	int len = str.length();
	for(int i=0; i<len; i++)
	{
		unsigned char c ;
		if( str[i] >= '0' && str[i] <= '9')
			c = str[i] - '0';
		else
			c = str[i] - 'A' + 10;

		if( (i+1) % 2 )
			arr[i/2] = arr[i/2] | ( c << 4);
		else
			arr[i/2] = arr[i/2] | c;

	}

	return arr;

}

string ascii_to_hex(unsigned char c)
{
	string ans = "";
	int left = (int)( (c&0xf0) >> 4);
	int right = (int)(c&0x0f);
	if( left < 10)
		ans = (char)(left+48);
	else
		ans = (char)(left+'A'-10);
	if(right < 10)
		ans += (char)(right+48);
	else
		ans += (char)(right+'A'-10);
	return ans;
}

unsigned char* permutation(unsigned char* arr,int table[],int table_size)
{
	unsigned char* permuted = new unsigned char[table_size/8];
	memset(permuted,0,table_size/8);
	for(int i=0;i<table_size;i++)
	{
		if( arr[(table[i]-1)/8] & ( 0x1 << ( 7 - ((table[i]-1)%8  )  )))
		{
			permuted[i/8] = permuted[i/8] | (0x1 << (7-i%8));
		}
	}
	return permuted;
}

void generate_keys(unsigned char* key,unsigned char** keys)
{
	unsigned char *key2 = new unsigned char[6];
	unsigned char *real_key = new unsigned char[6];

	memcpy(key2,permutation(key,PC1,56),7);
	
	cout<<"\nAfter initial permutation on key.\n";
	print(key2,7);
	cout<<endl;

	unsigned char *C = new unsigned char[4];	// left part of key
	unsigned char *D = new unsigned char[4];	// right part of key

	memcpy(C,key2,4);
	for(int i=0; i<4;i++)
	{
		memcpy(C,right_shift(C,4),4);
	}

	memcpy(D,key2+3,4);
	D[0] = D[0] & (0x0F);
	C[0] = C[0] & (0x0F);
	cout<<"\nLeft key part C0 : \n";
	print(C,4);
	cout<<"\nRight key part D0 : \n";
	print(D,4);
	cout<<endl;
    
    for(int i=0;i<16;i++)
    {
        for(int j=0;j<shifts_in_iterations[i];j++)
		{
			unsigned char c1,d1;
			c1 = (C[0] & 0x08) >> 3;
			d1 = (D[0] & 0x08) >> 3;
			memcpy(C,left_shift(C,4),4);
			C[3] = C[3] | c1;
			memcpy(D,left_shift(D,4),4);
			D[3] = D[3] | d1;
		}
		C[0] = C[0] & 0x0F;
		D[0] = D[0] & 0x0F;
		unsigned char *C_ = new unsigned char[4];
		memcpy(C_,C,4);
		memcpy(key2+3,D,4);
		for(int j=0;j<4;j++)
			memcpy(C_,left_shift(C_,4),4);
		memcpy(key2,C_,4);
		key2[3] = (C_[3]&0xF0) | (D[0]&0x0F);
		memcpy(real_key,permutation(key2,PC2,48),6);
		cout<<"\nKey "<<i+1<<" : \n";
		print(real_key,6);
        memcpy(keys[i],real_key,6);
    }

}

unsigned char* encrypt(unsigned char **keys, unsigned char *block)
{
	// now processing text
	cout<<"\n<===========================\tEncryption Starts From Here\t===============================>\n";
	memcpy(block,permutation(block,IP_,64),8);
	cout<<"\nIP\n";
	print(block,8);
	cout<<endl;

	unsigned char* L = new unsigned char[4];
	unsigned char* R = new unsigned char[4];
	
	memcpy(L,block,4);
	memcpy(R,block+4,4);

	for(int i=0;i<16;i++)
	{
		cout<<"\n----------------------------------------------------------- "<<i+1<<endl;
		
		// processing block
		cout<<"\nL(old) : ";
		print(L,4);
		cout<<"\nR (old) : ";
		print(R,4);
		cout<<endl;
		unsigned char* R_ = new unsigned char[6];
		memcpy(R_,permutation(R,E,48),6);
		cout<<"\nE(R) is : \n";
		print(R_,6);
		cout<<endl;

		unsigned char* f = new unsigned char[6];
		for(int j=0;j<6;j++)
		{
			f[j] = keys[i][j] ^ R_[j];
		}

		unsigned char* f_ = new unsigned char[4];
		memset(f_,0,4);
		for(int j=0;j<8;j++)
		{
			int bits = 8-(6*j)%8;
			unsigned char c = (f[6*j/8] & (0xfc >> ((6*j)%8))) << ( 8-bits );
			c = c >> 2;
			if( bits < 6)
				c = c | (f[6*(j+1)/8] >>  ( 8 -6 +bits));
			int row = (2* (int)(((c & 0x20))?1:0)) + (int) ((c & 0x01)?1:0);
			int col = (int)((c&0x1e) >> 1);
			unsigned char val = (unsigned char)(S[j][row][col]);
			if(j%2 == 0)
				val = val << 4;
			f_[j/2] = f_[j/2] | val;
		}
		
		cout<<"\nAfter substitiution\n";
		print(f_,4);
		cout<<endl;
		memcpy(f_,permutation(f_,P,32),4);
		cout<<"\nAfter applying  P\n";
		print(f_,4);
		cout<<endl;
		for(int j=0;j<4;j++)
		{
			f_[j] = f_[j] ^ L[j];
		}
		for(int j=0;j<4;j++)
		{
			L[j] = R[j];
			R[j] = f_[j];
		}
		cout<<"\nL(new) = ";
		print(L,4);
		cout<<"\nR(new) = ";
		print(R,4);
		cout<<endl;
	}

	unsigned char *crypted = new unsigned char[8];
	memcpy(crypted,R,4);
	memcpy(crypted+4,L,4);
	memcpy(crypted,permutation(crypted,IP_INV,64),8);

    return crypted;

}

int main()
{
	string block, key;
	cout<<"\nEnter block : ";
	getline(cin,block);
	cout<<"\nEnter key : ";
	getline(cin,key);

	cout<<"\nEnter PC1 : ";
	for(int i=0; i < 56; i++)
		cin>>PC1[i];
	
	cout<<"\nEnter shifts in iterations : ";
	for(int i=0;i<16;i++)
		cin>>shifts_in_iterations[i];
	
	cout<<"\nEnter PC2 : ";
	for(int i=0; i<48; i++)
		cin>>PC2[i];

	cout<<"\nEnter IP : ";
	for(int i=0;i<64;i++)
		cin>>IP_[i];
	
	cout<<"\nEnter E : ";
	for(int i=0;i<48;i++)
		cin>>E[i];

	cout<<"\nEnter substitution boxes : ";
	for(int i=0;i<8;i++)
	{
		cout<<"\nEnter box "<<i+1<<endl;
		for(int j=0;j<4;j++)
			for(int k=0;k<16;k++)
				cin>>S[i][j][k];
	}

	cout<<"\nEnter P : ";
	for(int i=0;i<32;i++)
		cin>>P[i];

	cout<<"\nEnter IP inverse : ";
	for(int i=0;i<64;i++)
		cin>>IP_INV[i];
	
	unsigned char *key_ = new unsigned char[ (key.length()+1)/2];
	unsigned char *block_ = new unsigned char[ (block.length()+1)/2];
    unsigned char **keys = new unsigned char*[16];
    for(int i=0;i<16;i++)
        keys[i] = new unsigned char[6];

	memcpy(key_,hex_to_ascii(key), (key.length()+1)/2);
	memcpy(block_,hex_to_ascii(block), (block.length()+1)/2);

    generate_keys(key_,keys);
 
	memcpy(block_,encrypt(keys,block_),8);
	cout<<"\nCrypted block is :\n";
	print(block_,8);
	cout<<endl;
	cout<<"\nCrypted block in HEX : \n";
	for(int i=0;i<8;i++)
		cout<<ascii_to_hex(block_[i])<<" ";
	cout<<endl;
    // reverse order of keys
       unsigned char* temp = new unsigned char[6];
    for(int i=0;i<8;i++)
    {
        memcpy(temp,keys[i],6);
        memcpy(keys[i],keys[15-i],6);
        memcpy(keys[15-i],temp,6);
    }
    memcpy(block_,encrypt(keys,block_),8);
	cout<<"\nDecrypted Block is :\n";
	print(block_,8);
	cout<<endl;
	cout<<"\nDecrypted block in HEX:\n";
	for(int i=0;i<8;i++)
		cout<<ascii_to_hex(block_[i])<<" ";
	cout<<endl;
	return 0;

}
