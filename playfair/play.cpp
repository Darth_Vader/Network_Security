#include <bits/stdc++.h>

using namespace std;

char mat[5][5]= {
		{'P','L','A','Y','F'},
		{'I','R','E','X','M'},
		{'B','C','D','G','H'},
		{'K','N','O','Q','S'},
		{'T','U','V','W','Z'}
		};

map<char,pair<int,int> > map_; 

string encrypt(string text)
{
	string e_string = "";
	
	for(int i=0;i<text.length();i++)
	{
		pair<int,int> c_a,c_b;
		c_a = map_[text[i++]];
		if ( i == text.length())
			c_b = map_['X'];
		else
			c_b = map_[text[i]];
		
		if( c_a == c_b )
		{
			i--;
			c_b = map_['X'];
		}

		if( c_a.first == c_b.first)
		{
			// same row
			e_string = e_string + mat[c_a.first][(c_a.second+1)%5] + mat[c_b.first][(c_b.second+1)%5];
		}
		else if( c_a.second == c_b.second)
		{
			// column same
			e_string = e_string + mat[(c_a.first+1)%5][c_a.second] + mat[(c_b.first+1)%5][c_b.second];
		}
		else
		{
			e_string = e_string+ mat[c_a.first][c_b.second] + mat[c_b.first][c_a.second];
		}
	
		
	}
	return e_string;

}

string decrypt(string text)
{
	string e_string = "";
	for(int i=0; i<text.length() ;i++)
	{
		pair<int,int> c_a, c_b;
		c_a = map_[text[i++]];
		if( i == text.length())
			c_b = map_['X'];
		else
			c_b = map_[text[i]];
	
		if( c_a.first == c_b.first)
		{
			// same row
			e_string = e_string + mat[c_a.first][(c_a.second+4)%5] + mat[c_b.first][(c_b.second+4)%5];
		}
		else if( c_a.second == c_b.second )
		{
			// column same
			e_string = e_string + mat[(c_a.first+4)%5][c_a.second] + mat[(c_b.first+4)%5][c_b.second];
		}
		else
		{
			e_string = e_string + mat[c_a.first][c_b.second] + mat[c_b.first][c_a.second];
		}
	}
	return e_string;
}

int main()
{
	cout<<"\nFor Wikipedia example copy input from input.txt file\n";
	char remaining_char;
	cout<<"\nEnter phrase to generate matrix (without space) : ";
	
	string phrase;
	getline(cin,phrase);

	for(int i=0; i<phrase.length(); i++)
	{
		if( phrase[i] >= 'a' )
			phrase[i] = phrase[i] - 'a' + 'A';
	}

	map<char,bool> checking;
	int i=0;
	for(int j=0; j < phrase.length() && i < 25;j++)
	{
		if( checking[phrase[j]] == false)
		{
			mat[(i)/5][(i)%5] = phrase[j];
			i++;
		}
		checking[phrase[j]] = true;
	}

		// matrix is not complete --------- full matrix by yourself
		for(char j='A'; j <= 'Z' ; j++)
		{
			if( checking[j] == false && i< 25)
			{
				mat[(i)/5][(i)%5] = j;
				i++;
			}
			else if( i == 25 && checking[j] == false)
			{
				remaining_char = j;
				break;	
			}
		}


	cout<<"\nMatrix is : \n";

	for(int j=0;j<5;j++)
	{
		for(int k=0;k<5;k++)
			cout<<mat[j][k]<<"\t";
		cout<<endl;
	}

	cout<<"\nRemaining character is : "<<remaining_char<<endl;

	string text;
	cout<<"\nEnter string (without space) : ";
	getline(cin,text);

	for(int i=0;i<text.length();i++)
	{
		if(text[i] > 90 )
			text[i] = text[i]-(97-65);
	}
	
	// loading map
	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			pair<int,int> x = make_pair(i,j);
			map_[mat[i][j]]=x;
		}
	}
	
	pair<int,int> x = make_pair(0,0);
	map_[remaining_char] = x;		// mapping remaining character to first element

	cout<<"\nEncrypted text is : \n";
	cout<<encrypt(text)<<endl<<endl;

	cout<<"\nDecrypted text is : \n";
	cout<<decrypt(encrypt(text))<<endl<<endl;
	return 0;

}
