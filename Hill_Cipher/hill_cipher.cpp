#include <bits/stdc++.h>

using namespace std;

void print(int*,int ,int);

int* multiply(int* mat1,int* mat2,int dim)
{
	int* result = new int[dim];
	for(int i=0; i<dim; i++)
	{
		result[i] = 0;
		for(int j=0; j<dim;j++)
			result[i] = ((result[i])%26 + ((mat1[i*dim+j])%26 * (mat2[j])%26)%26)%26;
	}
	return result;
}

void print(int* mat,int dim1,int dim2)
{
	for(int i=0;i<dim1;i++)
	{
		for(int j=0;j<dim2;j++)
			cout<<(mat[i*dim1+j])<<"\t";
		cout<<endl;
	}
}

int* encrypt(int* key, int* text,int dim, int text_len)
{
	int* temp = new int[text_len];
	for(int i=0; i<text_len/2;i++)
		memcpy(temp+i*dim, multiply(key,text + dim*i,dim),dim*sizeof(int));
	return temp;
}

int deter(int* mat,int dim)
{
	if( dim == 1)
	{
		int d = mat[0]%26;
		if(d<0)
			d = d+26;
		return d;
	}
	if(dim == 2)
	{
		int d = (((mat[0]*mat[3])%26 - (mat[1]*mat[2])%26)%26);
		while(d <0)
			d += 26;
		return d;
	}

	int* temp = new int[(dim-1)*(dim-1)];
	int det = 0;
	
	for(int i=0; i<1; i++)
	{
		for(int j=0; j<dim; j++)
		{
			int index = 0;
			for(int l=0;l<dim;l++)
			{
				for(int m=0;m<dim;m++)
				{
					if( i!=l && m!=j)
						temp[index++] = mat[l*dim+m];
				}
			}
		
			int det_ = deter(temp,dim-1);
			det_ = (det_*mat[i*dim+j])%26;
			if(( ((i%2)==0) ^ ((j%2)==0)))
				det_ = (-1)*det_;

			det_ = det_ % 26;
			while(det_ < 0)
				det_ += 26;
			det = (det + det_)%26;	

		}
	}
	return det;

}

int mul_inv(int n)
{
	for(int i=1;i<26;i++)
		if( (i*n)%26 == 1)
			return i;
	return -1;
}

int* inverse(int* mat,int dim)
{
	int det = deter(mat,dim);
	det = mul_inv(det);

	int* inv = new int[dim*dim];
	int* temp = new int[(dim-1)*(dim-1)];
	int index = 0;
	for(int i=0; i<dim; i++)
	{
		for(int j=0; j<dim; j++)
		{
			index = 0;
			for(int l=0; l<dim; l++)
				for(int m=0; m<dim; m++)
					if( i!=l && j!=m)
						temp[index++] = mat[l*dim+m];

			int temp_det = deter(temp,dim-1);
			if( (i%2==0) ^ (j%2==0))
				temp_det = temp_det*(-1);
			while( temp_det < 0)
				temp_det += 26;
			inv[j*dim+i] = (temp_det*det)%26;
		}
	}
	return inv;
}

int main()
{
	int tests;
	cout<<"\nEnter number of test cases : ";
	cin>>tests;
	for(int x=0;x<tests;x++)
	{
	cout<<"\n********************************************************************** Test Case = "<<x+1<<endl;
	string key, text;
	int dim;
	cout<<"\nEnter n : ";
	cin>>dim;
	cin.clear();
	cin.ignore();
	cout<<"\nEnter key (capital letters) : ";
	getline(cin,key);
	cout<<"\nEnter text (capital letters) : ";
	getline(cin,text);
	
	cout<<"\nKey = "<<key<<endl;
	cout<<"\nText = "<<text<<endl;

	int* key_ = new int[key.length()];
	int* text_ = new int[text.length()];
	
	for(int i=0; i<dim*dim; i++)
		key_[i] = key[i]-'A';
	for(int i=0; i<text.length(); i++)
		text_[i] = text[i] - 'A';


	cout<<"\nKey:\n";
	print(key_,dim,dim);
	cout<<"\nText : \n";
	print(text_,1,text.length());
	
	cout<<"\nEncryption -----------------------------------------------------\n";
	int *encrypted_text = new int[text.length()];
	memcpy(encrypted_text,encrypt(key_,text_,dim,text.length()), 4*text.length());
	for(int i=0; i<text.length(); i++)
		cout<<(char)(encrypted_text[i]+'A');
	cout<<endl;

	memcpy(key_,inverse(key_,dim),(dim*dim)*sizeof(int));
	cout<<"\nNow decrypting-------------------------------------------------\n";
	cout<<"\nInverse key is :\n";
	print(key_,dim,dim);
	cout<<endl;
	memcpy(encrypted_text,encrypt(key_,encrypted_text,dim,text.length()),4*text.length());
	for(int i=0; i<text.length();i++)
		cout<<(char)(encrypted_text[i]+'A');
	cout<<endl;
	cout<<endl;
	}
	return 0;
}
