#include <bits/stdc++.h>
#include <gmp.h>

using namespace std;

int main()
{
	cout<<"\nInput message should be only of capital letters because numerical convertion of text to number is coded only for capital letters which can be changed latter.\n";
	mpz_t one, ten, zero, temp, d, k, p, q, n, phi, e, msg,encrypted;
	mpz_inits(one,ten,zero,temp,d,k,p,q,n,phi,e,msg,encrypted,NULL);

	mpz_set_str(p,"61",10);
	mpz_set_str(one,"1",10);
	mpz_set_str(ten,"10",10);
	mpz_set_str(zero,"0",10);
	mpz_set_str(e,"2",10);
	mpz_set_str(k,"2",10);

	mpz_nextprime(p,p);
	mpz_nextprime(q,p);

	mpz_mul(n,p,q);
	mpz_sub(phi,p,one);
	mpz_sub(temp,q,one);
	mpz_mul(phi,phi,temp);

	while(true)
	{
		if( mpz_cmp(e,phi) >= 0 )
		{
			cout<<"\ne equals phi\n";
			exit(0);
		}
		mpz_gcd(temp,phi,e);
		if( mpz_cmp(temp,one) == 0)
			break;
		mpz_add(e,e,one);
	}



	mpz_mul(d,k,phi);
	mpz_add(d,d,one);
	mpz_invert(temp,e,phi);
	mpz_mul(d,d,temp);
	mpz_mod(d,d,phi);
	
	gmp_printf("\np = %Zd\tq = %Zd\t e = %Zd\td = %Zd\tn = %Zd\tphi = %Zd\n",p,q,e,d,n,phi);
	
	cout<<"\nEnter msg : ";
	string str, numerical_msg;
	getline(cin,str);
	for(int i=0; i<str.length(); i++)
	{
		int N = str[i];
		if( N == 0)
			numerical_msg = numerical_msg + "0";
		else
		{
			string X_N = "";
			while(N)
			{
				X_N = (char)( (N%10) + 48) + X_N;
				N = N / 10;
			}
			numerical_msg = numerical_msg + X_N;
		}
	}
	cout<<"\nMessage after numerical convertion ------------\n\n";
	cout<<numerical_msg<<endl;
	mpz_set_str(msg,numerical_msg.c_str(),10);
	numerical_msg = string(mpz_get_str(NULL,2,msg));
	
	int nn = (int)(mpz_sizeinbase(n,2));
	nn--;

	// get blocks of size nn from msg and make crypted msg

	if( numerical_msg.length() % nn != 0)
	{
		numerical_msg = string( nn- (numerical_msg.length()%nn),'0') + numerical_msg;
	}
//	cout<<"\nPadded bit string is : "<<numerical_msg<<endl;	

	int loops = ((numerical_msg.length() - 1)/ nn  + 1);
	string encrypted_msg = "";
//	cout<<"\nloops = "<<loops<<"\nblock size = "<<nn<<endl;
	for(int i=0; i<loops;i++)
	{
		string block = numerical_msg.substr(i*nn,nn);
		//cout<<"\nBlock = "<<block;
		// decrypt block
	//	cout<<"\nBlock["<<i<<"] = "<<block;
		mpz_set_str(temp,block.c_str(),2);
	//	gmp_printf("\ntemp = %Zd\n",temp);
		mpz_powm(temp,temp,e,n);
	//	gmp_printf("\nencrypted_temp : %Zd\n",temp);
		block = string(mpz_get_str(NULL,2,temp));
		// padding block to size n
	//	cout<<"\nBlock["<<i<<"] = "<<block;
		if( block.length() % (nn+1) != 0)
		{
			block =  string( (nn+1) - (block.length()%(nn+1)), '0') + block;
		}
	//	cout<<"\nencrypted block : "<<block<<endl;
		encrypted_msg = encrypted_msg + block;
	}

//	cout<<"\nEncrytped msg = " << encrypted_msg<<endl;
	mpz_set_str(temp,encrypted_msg.c_str(),2);
	gmp_printf("\nEncrypted message is -----------------\n\n%Zd\n",temp);
	nn++;
//	cout<<"\nDectyption block size = "<<nn<<endl;
	loops = encrypted_msg.length()/nn;
	string ans = "";
	for(int i=0; i<loops;i++)
	{
		string block = encrypted_msg.substr(i*nn,nn);
		mpz_set_str(temp,block.c_str(),2);	
		mpz_powm(temp,temp,d,n);
		block = string(mpz_get_str(NULL,2,temp));
		if( block.length() % (nn-1) != 0)
		{
			block =  string( (nn-1) - (block.length()%(nn-1)), '0') + block;
		}
	//	cout<<"\nBlock["<<i<<"] = "<<block;
		ans = ans + block;
	}
	//cout<<"\nDecrypted message = "<<ans<<endl;
	mpz_set_str(temp,ans.c_str(),2);
	//gmp_printf("\nDectyped msg : %Zd\n",temp);
	ans = string(mpz_get_str(NULL,10,temp));
	cout<<"\nDecrypted message is -------------------\n\n"<<ans<<endl;
	cout<<"\nDecrypted message in text format ------------------\n\n";
	// converting numerical number to text
	for(int i=0; i< ans.length(); i= i+2)
	{
		cout<<(char)( (ans[i]-48)*10 + ans[i+1]-48  );
	}
	cout<<endl;
	return 0;
}
