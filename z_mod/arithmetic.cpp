#include <iostream>
#include <gmp.h>
#include <cstring>

using namespace std;

string add(string a, string b)
{
	string res = "";
	int i=0,carry = 0;
	while( i < a.length() || i < b.length())
	{
		if ( i < a.length() && i<b.length())
		{
			res =(char) ((a[a.length()-i-1] + b[b.length()-i-1] + carry - 96)%10+ 48) + res;
			
			carry =  (a[a.length()-i-1] + b[b.length()-i-1] + carry - 96)/10;
		}
		else if( i < a.length())
		{
			res = (char)((a[a.length()-i-1] + carry - 48)%10 + 48) + res;
			carry = (a[a.length()-i-1] + carry - 48 )/10;
		}
		else 
		{
			res = (char)((b[b.length()-i-1] + carry - 48)%10 + 48) + res;
			carry =(b[b.length()-i-1] + carry - 48)/10;
		}
		i++;
	}
	if ( carry )
		res = (char)(carry + 48 ) + res;
	return res;
}

int compare(string num1, string num2)
{
	// find index of first non zero digit in both numbers
	int index1=0, index2 = 0;

	while( index1 < num1.length() && num1[index1] == '0')
		index1++;

	while(index2 < num2.length() && num2[index2] == '0' )
		index2++;

	// now start comparing

	while ( index1 < num1.length() && index2 < num2.length() && num1[index1] == num2[index2] )
	{
		index1++;
		index2++;
	}

	if( index1 == num1.length() && index2 == num2.length())
		return 0;
	if((num1.length() - index1) != (num2.length() - index2) )
		return (num1.length()-index1 - num2.length() + index2 );
	return num1[index1]-num2[index2];
			
}

string subtract(string a, string b)
{
	string ans = "";
	string small, large;
	int borrow=0, i =1;

	int result = compare(a,b);
	if( result == 0 )
	{
		return "0";
	}
	if ( result < 0 )
	{
		small = a;
		large = b;
	}
	else
	{
		large = a;
		small = b;
	}


	while ( (i-1) < small.length() )
	{
		int s = small[small.length()-i]-48, l = large[large.length()-i] - 48;
		if ( (l - borrow) < s)
		{
			ans = (char)( l + 10 - s - borrow + 48 ) + ans;
			borrow = 1;
		}

		else if ( l < s)
		{
			ans = (char)(l+10 - s + 48 - borrow) + ans;
			borrow = 1;
		}
		else 
		{
			ans = (char)( l - s + 48 - borrow ) + ans;
			borrow = 0;
		}
		i++;
	}	
	while ( (i-1) < large.length())
	{
		if( (large[large.length()-i] - 48 - borrow ) < 0)
		{
			ans = (char)(large[large.length()-i]+10-borrow) + ans;
			borrow = 1;
		}
		else
		{
			ans = (char)(large[large.length() - i] - borrow) + ans;
			borrow = 0;
		}
		i++;
	}

	return ans;
}

string mul(string a, string b)
{
	string ans = "0";
	for(int i=b.length()-1; i>=0 ;i--)
	{
		int carry = 0;
		string local_ans = "";
		for(int j=a.length()-1; j>=0; j--)
		{
			local_ans = (char)(((a[j]-48)*(b[i]-48) + carry )%10 + 48) + local_ans;
			carry = ((a[j]-48)*(b[i]-48) + carry)/10;
		}
		if( carry )
			local_ans = (char)(carry + 48) + local_ans;
		// put zeros on right to local ans
		for(int j=0;j<b.length()-i-1;j++)
			local_ans = local_ans + "0";
		// add local_ans with ans
		ans = add(ans,local_ans);
	}
	return ans;
}

string mod(string a,string b)
{
	while( compare(a,b) > 0 )
	{
		a = subtract(a,b);
	}
	if(compare(a,b) == 0)
		return "0";
	return a;
}

string mod_inverse(string a, string m)
{
	a = mod(a,m);
	for( string i = "1"; compare(i,m) < 0 ; i = add(i,"1"))
	{
		if( compare(mod( mul(a,i), m) , "1") == 0)
			return i;
	}
	return "undefined";
}

string add_inverse(string a, string m)
{
	a = mod(a,m);
	for( string i = "1"; compare(i,m)<0 ; i = add(i,"1"))
	{
		if( compare(mod(add(a,i),m) , "0" ) == 0)
			return i;
	}
	return "undefined";
}

int main()
{
	string a,b,m;

	mpz_t a_, b_, m_,temp_;
	mpz_inits(a_,b_,m_,temp_,NULL);


	cout<<"\nEnter a : ";
	getline(cin,a);
	cout<<"\nEnter b : ";
	getline(cin,b);
	cout<<"\nEnter m : ";
	getline(cin,m);

	mpz_set_str(a_,a.c_str(),10);
	mpz_set_str(b_,b.c_str(),10);
	mpz_set_str(m_,m.c_str(),10);

	cout<<"\nMod inverse of a w.r.t m  = "<< mod_inverse(a,m)<<endl<<endl;
	cout<<"\nAdditive inverse of a w.r.t. m = "<<add_inverse(a,m)<<endl<<endl;
	
	cout<<"\nMod a%b = "<<mod(a,b)<<endl;
	mpz_mod(temp_,a_,b_);
	gmp_printf("\nGMP mod a%b = %Zd\n",temp_);
	cout<<"\nMultiplication a*b  = "<<mul(a,b)<<endl;
	mpz_mul(temp_,a_,b_);
	gmp_printf("\nGMP Multiplication a*b = %Zd\n",temp_);

	mpz_mod(temp_,temp_,m_);
	cout<<"\nModular Multiplication (a*b)%m  = "<<mod(mul(a,b),m)<<endl;
	gmp_printf("\nGMP Modular Multiplication (a*b)\% m = %Zd\n",temp_);
	

	mpz_sub(temp_,a_,b_);
	cout<<"\nSubtraction ( larger - smaller ) = "<<subtract(a,b)<<endl;
	gmp_printf("\nGMP subtraction (don't consider sign) = %Zd\n",temp_);
	mpz_mod(temp_,temp_,m_);
	cout<<"\nModular Subtraction : "<<mod(subtract(a,b),m)<<endl;
	gmp_printf("\nGMP Modular Subtraction (don't consider sign) = %Zd\n",temp_);


	cout<<"\nAddition a+b = "<<add(a,b)<<endl;
	mpz_add(temp_,a_,b_);
	gmp_printf("\nGMP Addition a+b = %Zd\n",temp_);
	mpz_mod(temp_,temp_,m_);
	cout<<"\nModular Addition (a+b)%m  = "<<mod(add(a,b),m)<<endl<<endl;
	gmp_printf("\nGMP Modular Addition (a+b)\%m = %Zd\n",temp_);
	return 0;
}
