#include <bits/stdc++.h>
#include <gmp.h>
#include <unistd.h>
using namespace std;

mpz_t arr[100];
mpz_t p,q,n;

void init(mpz_t r)
{
    mpz_init(r);
    mpz_set_ui(r,0);
}

void input(mpz_t n)
{
    gmp_scanf("%Zd",n);
}

void output(mpz_t n)
{
    gmp_printf("%Zd\n",n);
}

void get_random(mpz_t n)
{
	gmp_randstate_t state;
    gmp_randinit_mt(state);
    unsigned long seed;
    seed = time(NULL);
    gmp_randseed_ui(state, seed);
    mpz_t limit;
    mpz_init(limit);
    mpz_set_ui(limit, 100000);
    mpz_urandomm(n, state, limit);
    gmp_randclear (state);
    sleep(1);
}

void get_prime(mpz_t n)
{
	mpz_t low;
	init(low);
	get_random(low);
	mpz_nextprime(n,low);
}

string gmp_to_string(mpz_t a){
	char buf [100];
	memset(buf,'\0',100);
	mpz_get_str(buf,10,a);
	return string(buf);

}
void get_public_key(mpz_t e,mpz_t n,mpz_t phi)
{
	while(1)
	{
		gmp_randstate_t state;
	    gmp_randinit_mt(state);
	    unsigned long seed;
	    seed = time(NULL);
	    gmp_randseed_ui(state, seed);
	    mpz_t limit;
	    mpz_init(limit);
	    mpz_set(limit,phi);
	    mpz_urandomm(e, state, limit);

	    mpz_t gcd;
	    init(gcd);
	    mpz_gcd(gcd,e,phi);
	    if(mpz_cmp_ui(gcd,1) == 0)
	    {
	    	break;
	    }
	    gmp_randclear (state);
	}
}

string encrypt(string plain,string publickey)
{
	mpz_t e;
	init(e);
	mpz_set_str(e,publickey.c_str(),10);

	string toreturn="";
	int len = plain.length();
	for(int i=0;i<100;i++)
		init(arr[i]);

	for(int i=0;i<len;i++)
	{
		int t = plain[i];
		mpz_set_ui(arr[i],t);
		mpz_powm(arr[i],arr[i],e,n);
		string temp=gmp_to_string(arr[i]);
		toreturn+=temp+"|";		
	}
	cout<<endl;
	toreturn+=".";
	return toreturn;
}

string decrypt(string cipher,string privatekey)
{

	mpz_t d;
	init(d);
	mpz_set_str(d,privatekey.c_str(),10);

	string str,temp;
	int i=0;
	mpz_t help;
	init(help);
	while(1)
	{
		if(cipher[i]=='.') break;
		while(cipher[i]!='|'){
			temp+=cipher[i];
			i++;
		}
		i++;
		mpz_set_str (help,temp.c_str(), 10);
		mpz_powm(help,help,d,n);
		int t = mpz_get_ui(help);
		char ch = t;
		str.push_back(ch);
		temp.clear();
	}
	return str;
}

void keygen(string &publickey,string &privatekey){
	init(p);
	init(q);
	get_prime(p);
	get_prime(q);
	cout<<"p : ";
	output(p);
	cout<<"q : ";
	output(q);

	init(n);
	mpz_mul(n,p,q);
	cout<<"n : ";
	output(n);

	mpz_t phi;
	init(phi);
	mpz_sub_ui(p,p,1);
	mpz_sub_ui(q,q,1);
	mpz_mul(phi,p,q);
	cout<<"phi(n) : ";
	output(phi);
	cout<<"\n";

	mpz_t e,d;
	init(e);
	get_public_key(e,n,phi);
	cout<<"e is : ";
	output(e);

	init(d);
	mpz_invert(d,e,phi);
	cout<<"d is : ";
	output(d);

	publickey=gmp_to_string(e);
	privatekey=gmp_to_string(d);

}

// int main(){
// 	keygen();

// 	string plain;
// 	cout<<"Enter plain text : ";
// 	cin>>plain;

// 	string cipher;
// 	cipher=encrypt_pt(plain);
// 	cout<<"cipher text :"<<cipher<<endl;	

// 	string res = decrypt_ct(cipher);
// 	cout<<res<<endl;

// }
