#include<bits/stdc++.h>
#include<gmp.h>
using namespace std;

char getchar(char fi, char se)
{
	int n = 0;
	n = fi-'0';
	n *= 10;
	n += se-'0';
	return char('a'+n-1);
}

void get_text(char* plaintext_str)
{
	int len = strlen(plaintext_str);
	int st = 0;
	if(len%2==1)
	{
		cout<<getchar('0',plaintext_str[0]);
		st++;
	}
	for(int i=st;i<len;i+=2)
	{
		cout<<getchar(plaintext_str[i],plaintext_str[i+1]);
	}
	cout<<endl;
}

string inttostr(int n)
{
    string res;

    if(n < 10)
    {
        res.push_back('0');
        res.push_back(n+'0');
        return res;
    }

    while(n)
    {
        res.push_back(n%10+'0');
        n /= 10;
    }
   
    reverse(res.begin(), res.end());
    return res;
}

void find_e(mpz_t e, mpz_t phi)
{
    mpz_t gcd;
    mpz_init(gcd);
    mpz_set_ui(e,2);

    while(mpz_cmp(phi,e) > 0)
    {
        mpz_gcd(gcd,phi,e);
        if(mpz_cmp_si(gcd,1)==0)
            return;
        mpz_add_ui(e,e,1);
    }
}

void key_gen(string &pub, string &prv, long long seed)
{
    mpz_t randlim,n,p,q,phi,e,d;

    mpz_inits(randlim,n,p,q,phi,e,d,NULL);

    mpz_set_str(randlim,"100000000000000000000",10);

    gmp_randstate_t state;
    gmp_randinit_mt(state);
    gmp_randseed_ui(state,seed);
    mpz_urandomm(p,state,randlim);
    gmp_randseed(state,p);
	mpz_nextprime(p,p);
    mpz_urandomm(q,state,randlim);
	mpz_nextprime(q,q);
    mpz_mul(n,p,q);
    mpz_sub_ui(p,p,1);
    mpz_sub_ui(q,q,1);

    mpz_mul(phi,p,q);
    find_e(e,phi);

    int ret = mpz_invert(d,e,phi);
    if(ret == 0)
    {
        cout<<"Error in finding d!"<<endl;
        exit(0);
    }

    char* ec;
    char* dc;
    char* nc;
    ec = (char*) malloc(1000*sizeof(char));
    dc = (char*) malloc(1000*sizeof(char));
    nc = (char*) malloc(1000*sizeof(char));

    mpz_get_str(ec,10,e);
    mpz_get_str(dc,10,d);
    mpz_get_str(nc,10,n);

    string estr(ec);
    string dstr(dc);
    string nstr(nc);
    
    pub = nstr;
    pub.push_back('|');
    pub = pub+estr;
    prv = nstr;
    prv.push_back('|');
    prv = prv+dstr;
}

string encrypt(char* data, string key)
{
    string p = "",n = "";
    int i=0;
    for(;key[i]!='|';i++)
    {
        n += key[i];
    }
    i++;
    for(;i<(int)key.length()&&key[i]>=48&&key[i]<=57;i++)
    {
        p += key[i];
    }
    char* pc;
    char* nc;

    pc = (char*) malloc(1000*sizeof(char));
    nc = (char*) malloc(1000*sizeof(char));

    pc = (char*)p.c_str();
    nc = (char*)n.c_str();

    mpz_t pm, nm,M,C;
    mpz_inits(pm,nm,M,C,NULL);

    mpz_set_str(pm,pc,10);
    mpz_set_str(nm,nc,10);
    mpz_set_str(M,data,10);

    mpz_powm(C,M,pm,nm);
    mpz_get_str(data,10,C);
    return string(data);
}

string decrypt(char* data, string key)
{
    string p = "",n = "";
    int i=0;
    for(;key[i]!='|';i++)
    {
        n += key[i];
    }
    i++;
    for(;i<(int)key.length()&&key[i]>=48&&key[i]<=57;i++)
    {
        p += key[i];
    }
    cout<<"p,n = "<<p<<" "<<n<<endl;
    char* pc = (char*) malloc(1000*sizeof(char));
    char* nc = (char*) malloc(1000*sizeof(char));

    pc = (char*)p.c_str();
    nc = (char*)n.c_str();

    mpz_t pm, nm,M,C;
    mpz_inits(pm,nm,M,C,NULL);

    mpz_set_str(pm,pc,10);
    mpz_set_str(nm,nc,10);
    mpz_set_str(M,data,10);
    mpz_powm(C,M,pm,nm);
    mpz_get_str(data,10,C);
    return string(data);
}

