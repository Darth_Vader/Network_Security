#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/csma-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/netanim-module.h"
#include <bits/stdc++.h>
#include "rsaProxy.cpp"

using namespace ns3;
using namespace std;

string publickey,privatekey;
NS_LOG_COMPONENT_DEFINE ("SocketBoundRoutingExample");
Ipv4InterfaceContainer interfaces;
uint16_t dst1port,dst2port;

void SendStuff (char *data,Ptr<Socket> sock, Ipv4Address dstaddr, uint16_t port)
{
  Ptr<Packet> p = Create<Packet> ();
  uint8_t buffer[1000];
  bzero((char*)buffer,100);
  uint32_t size;
  strcpy((char*)buffer,data);
  size = strlen((char*)buffer);
  Ptr<Packet> p1 = Create<Packet> (buffer,size);
  p->AddAtEnd(p1);
  sock->SendTo (p, 0, InetSocketAddress (dstaddr,port));
  return;
}

void srcSocketRecv (Ptr<Socket> socket)
{

}

void dst1SocketRecv (Ptr<Socket> socket)
{
  Address from;
  Ptr<Packet> packet = socket->RecvFrom (from);
  cout<<"Packet:"<<*packet<<" SIZE:"<<packet->GetSize()<<endl;
  packet->RemoveAllPacketTags ();
  packet->RemoveAllByteTags ();
  unsigned char buffer[1000];
  memset(buffer,'\0',1000);
  packet->CopyData(buffer,packet->GetSize());
  InetSocketAddress address = InetSocketAddress::ConvertFrom (from);
  cout<<"Destination 1 Received " << packet->GetSize () << " bytes from " << address.GetIpv4 () << " Data:" <<buffer<<endl;

  //Sending message to Destination 2

  char data[1000];
  memset(data,'\0',1000);
  strcpy(data,(char*)buffer);

  //Encrypting data
  string cipher=encrypt(string(data),publickey);

  cout<<"Sending message to Destination 2"<<endl;

  memset(data,'\0',1000);
  strcpy(data,cipher.c_str());
  SendStuff (data, socket,interfaces.GetAddress(2), dst2port);
}

void dst2SocketRecv(Ptr<Socket> socket){
  Address from;
  Ptr<Packet> packet = socket->RecvFrom (from);
  cout<<"Packet:"<<*packet<<" SIZE:"<<packet->GetSize()<<endl;
  packet->RemoveAllPacketTags ();
  packet->RemoveAllByteTags ();
  unsigned char buffer[1000];
  memset(buffer,'\0',1000);
  packet->CopyData(buffer,packet->GetSize());
  InetSocketAddress address = InetSocketAddress::ConvertFrom (from);

  //Decrypting cipher text
  string cipher=string((char*)buffer);
  string text=decrypt(cipher,privatekey);
  cout<<"Destination 2 Received " << packet->GetSize () << " bytes from " << address.GetIpv4 () << " Decrypted Data:" <<text<<endl;

}

int main (int argc, char *argv[])
{
  keygen(publickey,privatekey);

  NodeContainer nodes;
  nodes.Create (3);

  CsmaHelper csma;
  csma.SetChannelAttribute ("DataRate", StringValue ("100Mbps"));
  csma.SetChannelAttribute ("Delay", TimeValue (NanoSeconds (6560)));

  NetDeviceContainer CsmaDevices;
  CsmaDevices = csma.Install (nodes);

  InternetStackHelper stack;
  stack.Install (nodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.2.0", "255.255.255.0");
  interfaces = address.Assign (CsmaDevices);

  Ipv4GlobalRoutingHelper:: PopulateRoutingTables();
  
  dst1port = 8080;
  dst2port = 8081;

  //Sending message
  Ptr<Socket> srcSocket = Socket::CreateSocket (nodes.Get(0), TypeId::LookupByName ("ns3::UdpSocketFactory"));
  srcSocket->Bind ();
  srcSocket->SetRecvCallback (MakeCallback (&srcSocketRecv));

  Ptr<Socket> dst1Socket = Socket::CreateSocket (nodes.Get(1), TypeId::LookupByName ("ns3::UdpSocketFactory"));
  Ipv4Address dst1addr ( interfaces.GetAddress(1) );
  InetSocketAddress dst1 = InetSocketAddress (dst1addr, dst1port);
  dst1Socket->Bind (dst1);
  dst1Socket->SetRecvCallback (MakeCallback (&dst1SocketRecv));

  Ptr<Socket> dst2Socket = Socket::CreateSocket (nodes.Get(2), TypeId::LookupByName ("ns3::UdpSocketFactory"));
  Ipv4Address dst2addr ( interfaces.GetAddress(2) );
  InetSocketAddress dst2 = InetSocketAddress (dst2addr, dst2port);
  dst2Socket->Bind (dst2);
  dst2Socket->SetRecvCallback (MakeCallback (&dst2SocketRecv));


  char data[1000];
  memset(data,'\0',1000);
  string tosend="Welcome to CNS Programming!!!";
  strcpy(data,tosend.c_str());
 
  Packet::EnablePrinting();


  // AnimationInterface anim ("Q1.xml");
  // anim.SetConstantPosition (nodes.Get(0), 1.0, 10.0);
  // anim.SetConstantPosition (nodes.Get(1), 8.0, 10.0);
  // anim.SetConstantPosition (nodes.Get(2), 15.0, 10.0);

  Simulator::Schedule (Seconds (1),&SendStuff, data, srcSocket, dst1addr, dst1port);

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
