/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "src/core/model/callback.h"
#include "src/network/utils/packet-socket.h"
#include "ns3/netanim-module.h"
#include "/home/darth/headers/string_packet.h"
#include "/home/darth/headers/s_encryption.h"

using namespace ns3;

NS_LOG_COMPONENT_DEFINE ("Kerberos");

#define PORT 9999

enum indexes{C,AS};

class server
{
  Ptr<Socket> socket;
  TypeId tid;
  void send_packet()
  {
    std::cout<<"\nserver sending packet\n";
    Ptr<Packet> p = Create<Packet> (1024);
     socket->Send(p);
  }

public:
  server()
  {
    tid = TypeId::LookupByName("ns3::UdpSocketFactory");
    
  }
  void Recv(Ptr<Socket> socket)
  {
    std::cout<<"\nPacket received by client.\n";
    Address add;
    Ptr<Packet> p = socket->RecvFrom(add);
    socket->SendTo(p,0,add);
    std::cout<<"\nPacket sent by server. as reply\n";
  }
  void start(Ptr<Node> node)
  {
    socket = Socket::CreateSocket(node,tid);
    InetSocketAddress local = InetSocketAddress(Ipv4Address::GetAny(),PORT);
    if( socket->Bind(local) != 0)
        std::cout<<"\nError in binding server.\n";
    socket->SetRecvCallback(MakeCallback(&server::Recv,this));
  }
  void send()
  {
    Simulator::ScheduleNow (&server::send_packet,this);
  }
};


class client
{
  Ptr<Socket> socket;
  TypeId tid;
  void send_packet()
{
  std::cout<<"\nClient sending packet\n";
  Ptr<Packet> p = Create<Packet> (1024);
   socket->Send(p);
}

public:
  client()
  {
    tid = TypeId::LookupByName("ns3::UdpSocketFactory");
    
  }
  void Recv(Ptr<Socket> socket)
  {
    std::cout<<"\nPacket received by client.\n";
    Address add;
    Ptr<Packet> p = socket->RecvFrom(add);
    socket->SendTo(p,0,add);
    std::cout<<"\nPacket sent by client. as reply\n";
  }
  void start(Ptr<Node> node,Ipv4Address remote_addr)
  {
    socket = Socket::CreateSocket(node,tid);
    InetSocketAddress remote = InetSocketAddress(remote_addr,PORT);
    if( socket->Connect(remote) != 0)
      std::cout<<"\nError in connection to remote.\n";
    if( socket->Bind() != 0)
        std::cout<<"\nError in binding client.\n";
    std::cout<<"\nClient Connected\n";
    socket->SetRecvCallback(MakeCallback(&client::Recv,this));
  }
  void send()
  {
    Simulator::ScheduleNow (&client::send_packet,this);
  }
};


int
main (int argc, char *argv[])
{
  CommandLine cmd;
  cmd.Parse (argc, argv);
  
  Time::SetResolution (Time::NS);
  LogComponentEnable ("UdpEchoClientApplication", LOG_LEVEL_INFO);
  LogComponentEnable ("UdpEchoServerApplication", LOG_LEVEL_INFO);

  NodeContainer nodes;
  nodes.Create (2);

  PointToPointHelper pointToPoint;
  pointToPoint.SetDeviceAttribute ("DataRate", StringValue ("5Mbps"));
  pointToPoint.SetChannelAttribute ("Delay", StringValue ("2ms"));

  NetDeviceContainer devices;
  devices = pointToPoint.Install (nodes);

  InternetStackHelper stack;
  stack.Install (nodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  Ipv4InterfaceContainer interfaces = address.Assign (devices);



  // From here

  NS_LOG_INFO ("Create sockets.");
  
  server *s = new server();
  s->start(nodes.Get(AS));

  client *c = new client();
  c->start(nodes.Get(C),interfaces.GetAddress(AS));
  c->send();



  // AnimationInterface anim("assignment1.xml");

  // anim.SetConstantPosition(nodes.Get(0),1.0,2.0);
  // anim.SetConstantPosition(nodes.Get(1),5.0,10.0);

  Simulator::Run ();
  Simulator::Destroy ();
  return 0;
}
